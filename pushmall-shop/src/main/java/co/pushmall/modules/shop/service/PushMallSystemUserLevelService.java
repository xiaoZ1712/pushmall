package co.pushmall.modules.shop.service;

import co.pushmall.modules.shop.domain.PushMallSystemUserLevel;
import co.pushmall.modules.shop.service.dto.PushMallSystemUserLevelDTO;
import co.pushmall.modules.shop.service.dto.PushMallSystemUserLevelQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2019-12-04
 */
//@CacheConfig(cacheNames = "yxSystemUserLevel")
public interface PushMallSystemUserLevelService {

    /**
     * 查询数据分页
     *
     * @param criteria
     * @param pageable
     * @return
     */
    //@Cacheable
    Map<String, Object> queryAll(PushMallSystemUserLevelQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria
     * @return
     */
    //@Cacheable
    List<PushMallSystemUserLevelDTO> queryAll(PushMallSystemUserLevelQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    //@Cacheable(key = "#p0")
    PushMallSystemUserLevelDTO findById(Integer id);

    /**
     * 创建
     *
     * @param resources
     * @return
     */
    //@CacheEvict(allEntries = true)
    PushMallSystemUserLevelDTO create(PushMallSystemUserLevel resources);

    /**
     * 编辑
     *
     * @param resources
     */
    //@CacheEvict(allEntries = true)
    void update(PushMallSystemUserLevel resources);

    /**
     * 删除
     *
     * @param id
     */
    //@CacheEvict(allEntries = true)
    void delete(Integer id);
}
