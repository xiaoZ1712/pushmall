package co.pushmall.modules.shop.service.dto;

import co.pushmall.annotation.Query;
import lombok.Data;

/**
 * @author pushmall
 * @date 2019-10-03
 */
@Data
public class PushMallStoreCategoryQueryCriteria {

    // 模糊
    @Query(type = Query.Type.INNER_LIKE)
    private String cateName;

    @Query
    private Integer isDel = 0;
}
