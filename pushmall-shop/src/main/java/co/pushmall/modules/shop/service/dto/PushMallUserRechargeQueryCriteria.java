package co.pushmall.modules.shop.service.dto;

import co.pushmall.annotation.Query;
import lombok.Data;

/**
 * @author pushmall
 * @date 2020-03-02
 */
@Data
public class PushMallUserRechargeQueryCriteria {

    /**
     * 模糊
     */
    @Query(type = Query.Type.INNER_LIKE)
    private String nickname;
}
