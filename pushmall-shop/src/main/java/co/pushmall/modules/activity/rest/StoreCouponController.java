package co.pushmall.modules.activity.rest;

import co.pushmall.aop.log.Log;
import co.pushmall.modules.activity.domain.PushMallStoreCoupon;
import co.pushmall.modules.activity.service.PushMallStoreCouponService;
import co.pushmall.modules.activity.service.dto.PushMallStoreCouponQueryCriteria;
import co.pushmall.utils.OrderUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author pushmall
 * @date 2019-11-09
 */
@Api(tags = "商城:优惠券管理")
@RestController
@RequestMapping("api")
public class StoreCouponController {

    private final PushMallStoreCouponService pushMallStoreCouponService;

    public StoreCouponController(PushMallStoreCouponService pushMallStoreCouponService) {
        this.pushMallStoreCouponService = pushMallStoreCouponService;
    }

    @Log("查询")
    @ApiOperation(value = "查询")
    @GetMapping(value = "/PmStoreCoupon")
    @PreAuthorize("@el.check('admin','YXSTORECOUPON_ALL','YXSTORECOUPON_SELECT')")
    public ResponseEntity getPushMallStoreCoupons(PushMallStoreCouponQueryCriteria criteria, Pageable pageable) {
        return new ResponseEntity(pushMallStoreCouponService.queryAll(criteria, pageable), HttpStatus.OK);
    }

    @Log("新增")
    @ApiOperation(value = "新增")
    @PostMapping(value = "/PmStoreCoupon")
    @PreAuthorize("@el.check('admin','YXSTORECOUPON_ALL','YXSTORECOUPON_CREATE')")
    public ResponseEntity create(@Validated @RequestBody PushMallStoreCoupon resources) {
        resources.setAddTime(OrderUtil.getSecondTimestampTwo());
        return new ResponseEntity(pushMallStoreCouponService.create(resources), HttpStatus.CREATED);
    }

    @Log("修改")
    @ApiOperation(value = "修改")
    @PutMapping(value = "/PmStoreCoupon")
    @PreAuthorize("@el.check('admin','YXSTORECOUPON_ALL','YXSTORECOUPON_EDIT')")
    public ResponseEntity update(@Validated @RequestBody PushMallStoreCoupon resources) {
        pushMallStoreCouponService.update(resources);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @Log("删除")
    @ApiOperation(value = "删除")
    @DeleteMapping(value = "/PmStoreCoupon/{id}")
    @PreAuthorize("@el.check('admin','YXSTORECOUPON_ALL','YXSTORECOUPON_DELETE')")
    public ResponseEntity delete(@PathVariable Integer id) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        PushMallStoreCoupon resources = new PushMallStoreCoupon();
        resources.setId(id);
        resources.setIsDel(1);
        pushMallStoreCouponService.update(resources);
        return new ResponseEntity(HttpStatus.OK);
    }
}
