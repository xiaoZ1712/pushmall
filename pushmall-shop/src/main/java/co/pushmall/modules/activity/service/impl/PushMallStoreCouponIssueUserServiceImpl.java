package co.pushmall.modules.activity.service.impl;

import co.pushmall.modules.activity.domain.PushMallStoreCouponIssueUser;
import co.pushmall.modules.activity.repository.PushMallStoreCouponIssueUserRepository;
import co.pushmall.modules.activity.service.PushMallStoreCouponIssueUserService;
import co.pushmall.modules.activity.service.dto.PushMallStoreCouponIssueUserDTO;
import co.pushmall.modules.activity.service.dto.PushMallStoreCouponIssueUserQueryCriteria;
import co.pushmall.modules.activity.service.mapper.PushMallStoreCouponIssueUserMapper;
import co.pushmall.utils.PageUtil;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author pushmall
 * @date 2019-11-09
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallStoreCouponIssueUserServiceImpl implements PushMallStoreCouponIssueUserService {

    private final PushMallStoreCouponIssueUserRepository pushMallStoreCouponIssueUserRepository;

    private final PushMallStoreCouponIssueUserMapper pushMallStoreCouponIssueUserMapper;

    public PushMallStoreCouponIssueUserServiceImpl(PushMallStoreCouponIssueUserRepository pushMallStoreCouponIssueUserRepository, PushMallStoreCouponIssueUserMapper pushMallStoreCouponIssueUserMapper) {
        this.pushMallStoreCouponIssueUserRepository = pushMallStoreCouponIssueUserRepository;
        this.pushMallStoreCouponIssueUserMapper = pushMallStoreCouponIssueUserMapper;
    }

    @Override
    public Map<String, Object> queryAll(PushMallStoreCouponIssueUserQueryCriteria criteria, Pageable pageable) {
        Page<PushMallStoreCouponIssueUser> page = pushMallStoreCouponIssueUserRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);
        return PageUtil.toPage(page.map(pushMallStoreCouponIssueUserMapper::toDto));
    }

    @Override
    public List<PushMallStoreCouponIssueUserDTO> queryAll(PushMallStoreCouponIssueUserQueryCriteria criteria) {
        return pushMallStoreCouponIssueUserMapper.toDto(pushMallStoreCouponIssueUserRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    public PushMallStoreCouponIssueUserDTO findById(Integer id) {
        Optional<PushMallStoreCouponIssueUser> yxStoreCouponIssueUser = pushMallStoreCouponIssueUserRepository.findById(id);
        ValidationUtil.isNull(yxStoreCouponIssueUser, "PushMallStoreCouponIssueUser", "id", id);
        return pushMallStoreCouponIssueUserMapper.toDto(yxStoreCouponIssueUser.get());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PushMallStoreCouponIssueUserDTO create(PushMallStoreCouponIssueUser resources) {
        return pushMallStoreCouponIssueUserMapper.toDto(pushMallStoreCouponIssueUserRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallStoreCouponIssueUser resources) {
        Optional<PushMallStoreCouponIssueUser> optionalPushMallStoreCouponIssueUser = pushMallStoreCouponIssueUserRepository.findById(resources.getId());
        ValidationUtil.isNull(optionalPushMallStoreCouponIssueUser, "PushMallStoreCouponIssueUser", "id", resources.getId());
        PushMallStoreCouponIssueUser pushMallStoreCouponIssueUser = optionalPushMallStoreCouponIssueUser.get();
        pushMallStoreCouponIssueUser.copy(resources);
        pushMallStoreCouponIssueUserRepository.save(pushMallStoreCouponIssueUser);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        pushMallStoreCouponIssueUserRepository.deleteById(id);
    }
}
