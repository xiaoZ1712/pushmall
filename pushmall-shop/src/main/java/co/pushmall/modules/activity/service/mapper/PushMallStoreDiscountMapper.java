package co.pushmall.modules.activity.service.mapper;

import co.pushmall.mapper.EntityMapper;
import co.pushmall.modules.activity.domain.PushMallStoreDiscount;
import co.pushmall.modules.activity.service.dto.PushMallStoreDiscountDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author pushmall
 * @date 2019-12-22
 */
@Mapper(componentModel = "spring", uses = {}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PushMallStoreDiscountMapper extends EntityMapper<PushMallStoreDiscountDTO, PushMallStoreDiscount> {

}
